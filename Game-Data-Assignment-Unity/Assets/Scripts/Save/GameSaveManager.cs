﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

	public string gameDataFilename = "game-save.json";
	private string gameDataFilePath;
	public List<string> saveItems;
	public bool firstPlay = true;

	// Singleton pattern from:
	// http://clearcutgames.net/home/?p=437

	// Static singleton property
	public static GameSaveManager Instance { get; private set; }



	//  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}


	void Awake() {
		// First we check if there are any other instances conflicting
		if (Instance != null && Instance != this) {
			// If that is the case, we destroy other instances
			Destroy(gameObject);
			return;
		}


		// Here we save our singleton instance
		Instance = this;

		// Furthermore we make sure that we don't destroy between scenes (this is optional)
		DontDestroyOnLoad(gameObject);

		gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
		saveItems = new List<string>();
	}


	public void AddObject(string item) {
		saveItems.Add(item);
	}

	//get all objects to be saved 
	public void Save() {
		saveItems.Clear();

		Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
		foreach (Save saveableObject in saveableObjects) {
			saveItems.Add(saveableObject.Serialize());
		}
		//write the data to a text file
		using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
			foreach (string item in saveItems) {
				gameDataFileStream.WriteLine(item);
			}
		}
	}

	// first time playing to false, clear saveitems, load the scene
	public void Load() {
		firstPlay = false;
		saveItems.Clear();
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	//if not first play run the LoadSaveGameData, DestroyAllSaveableObjectsInScene and CreateGameObjects functions
	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		if (firstPlay) return;
		LoadSaveGameData();
		DestroyAllSaveableObjectsInScene();
		CreateGameObjects();
	}

	//load save data and update the scene
	//create a new reader that can read our file
	void LoadSaveGameData() {
		using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
			while (gameDataFileStream.Peek() >= 0) {
				string line = gameDataFileStream.ReadLine().Trim();
				if (line.Length > 0) {
					saveItems.Add(line);
				}
			}
		}
	}

	// store all savable objects in an array
	//loop through and destroy all gameobjects marked as savable objects in the scene
	void DestroyAllSaveableObjectsInScene() {
		Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
		foreach (Save saveableObject in saveableObjects) {
			Destroy(saveableObject.gameObject);
		}
	}


	// search the saveItems array 
	//string pattern = "prefabName":"
	// set int patternIndex to the zero-based index of the first occurrence of the specified string in the current String object
	//set int valuestartindex
	//set prefab name to a retrieved substring from this instance. The substring starts at a specified character position and has a specified length.
	//Load assets stored at path in a Resources folder as gameobjects
	//for each item call the Deserialize method with a value of saveitem
	void CreateGameObjects() {
		foreach (string saveItem in saveItems) {
			string pattern = @"""prefabName"":""";
			int patternIndex = saveItem.IndexOf(pattern);
			int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
			int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
			string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

			GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
			item.SendMessage("Deserialize", saveItem);
		}
	}
}
