﻿
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[Serializable]
public class DroneSave : Save {

	public Data data;
	private drone drone;
	private string jsonString;

	//The Serializable attribute lets you embed a class with sub properties in the inspector, display variables in the inspectorg
	[Serializable]
	//the properties we want to save for the drone in the Base Data script. Its position, rotation and the direction it was travelling
	public class Data : BaseData {
		public float energy;
		public Color droneColor;
		public Vector3 curPos;
		public float speed;
		public bool dirRight;
	}

	void Awake() {
		drone = GetComponent<drone>();
		data = new Data();
	}



	public override string Serialize() {
		data.prefabName = prefabName;
		data.energy = drone.energy;
		data.droneColor = drone.droneColor;
		data.curPos = drone.transform.position;
		data.speed = drone.speed;
		data.dirRight = drone.dirRight;
		jsonString = JsonUtility.ToJson(data);
		return (jsonString);
	}



	public override void Deserialize(string jsonData) {
		JsonUtility.FromJsonOverwrite(jsonData, data);
		drone.energy = data.energy;
		drone.GetComponent<Renderer> ().material.color = data.droneColor;
		drone.transform.position = data.curPos;
		drone.speed = data.speed;
		drone.dirRight = data.dirRight;
		drone.name = "drone";
	}
}
