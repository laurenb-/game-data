﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

    public Data data;
    private Tank tank;
    private string jsonString;

	//The Serializable attribute lets you embed a class with sub properties in the inspector, display variables in the inspectorg
	[Serializable]
	//the properties we want to save for the tank in the Base Data script. Its position, rotation and the direction it was travelling
	public class Data : BaseData {
		public Vector3 position;
		public Vector3 eulerAngles;
		public Vector3 destination;
	}

	//on awake get the tank script
	// create a new place to store arbitrary data associated with the matched elements
	void Awake() {
		tank = GetComponent<Tank>();
		data = new Data();
	}

	//set and store the string from the save script which is the prefab name
	//set and store the current transform position  of the tank
	//set and store the current rotation of the tank
	//set and store the current destination of the tank
	//Generate a JSON representation of the data
	//return the values for JSON string

	public override string Serialize() {
		data.prefabName = prefabName;
		data.position = tank.transform.position;
		data.eulerAngles = tank.transform.eulerAngles;
		data.destination = tank.destination;
		jsonString = JsonUtility.ToJson(data);
		return (jsonString);
	}

	//Overwrite data in the data object by reading from its JSON representation.
	//loads the JSON data and update the values stored in it

	public override void Deserialize(string jsonData) {
		JsonUtility.FromJsonOverwrite(jsonData, data);
		tank.transform.position = data.position;
		tank.transform.eulerAngles = data.eulerAngles;
		tank.destination = data.destination;
		tank.name = "Tank";
	}
}