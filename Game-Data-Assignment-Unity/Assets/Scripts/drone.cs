﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class drone : MonoBehaviour {


	public enum State {
		Patrolling,
		Resting,
		Chasing
	}

	public State state;

	public float patrolSpeed;
	public float chaseSpeed;

	public float energy;
	public float maxEnergy;
	public float energyDepletionRate;
	public float energyRegenerationRate;
	public Color droneColor;
	public Color curDroneColor;

	public bool dirRight = true;
	public float speed;



	// Update is called once per frame
	void Update() {
		
		switch (state) {
		case State.Patrolling:
			Patrol();
			break;
		case State.Resting:
			Rest();
			break;

		}
	}


	void Patrol() {
		//transform.position = Vector3.Lerp(pos1, pos2, Mathf.PingPong(Time.time * speed, 1f));
		if (dirRight)
			transform.Translate (Vector2.right * speed * Time.deltaTime);
		else
			transform.Translate (-Vector2.right * speed * Time.deltaTime);

		if (transform.position.x >= 4.0f){
			dirRight = false;
		}

		if (transform.position.x <= -4.0f){
			dirRight = true;
		}
		DepleteEnergy();
	}
		


	void Rest() {
		energy += energyRegenerationRate * Time.deltaTime;
		this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
		droneColor = Color.blue;
		this.GetComponent<Renderer> ().material.color = droneColor;
		if (energy >= maxEnergy) {
			energy = maxEnergy;
			state = State.Patrolling; 
			droneColor = Color.white;
			this.GetComponent<Renderer> ().material.color = droneColor;
		}
	}


	void DepleteEnergy() {
		energy -= energyDepletionRate * Time.deltaTime;

		if (energy <= 0) {
			energy = 0;
			state = State.Resting;
		}

	}
		

}
