﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cactus : MonoBehaviour {
	public AudioClip damage;

	void Start(){

		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = damage;
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){
			other.GetComponentInChildren<Renderer> ().material.color = Color.magenta;
			GetComponent<AudioSource> ().Play ();
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Player"){
			other.GetComponentInChildren<Renderer> ().material.color = Color.white;
		}
	}
}
