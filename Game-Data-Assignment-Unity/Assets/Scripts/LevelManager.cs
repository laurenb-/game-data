﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    public Transform groundHolder;
    public Transform turretHolder;
	public Transform cactiiHolder;

    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;
    public Texture2D spriteSheetTexture;

    public GameObject tilePrefab;
    public GameObject turretPrefab;
	public GameObject cactusPrefab;

    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
	public List<Vector3> cactusPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    public string TMXFilename;

    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    public int pixelsPerUnit = 32;

    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;



	//clearing the console/output window of debug messages as a shortcut instead of having to click the clear button manually
    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
		//name of the assembly this type was loaded from
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditorInternal.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }



    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET

    public void LoadLevel() {

        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);

		// Find the tmx file called Maps in the streaming assets folder, store this file in the variable named TMXFile, the combine function concatenates two path strings.
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // clear the map data list, set the string called content to all the data in the TMXFile, XML reader is a forward only access to the XML data , it grabs the XML data from the script
		//get info from the map node, convert the width and height values from the map node to integers
		//get info from the tileset node, convert the tilewidth, tileheight, tilecount and columns values from the tileset node to integers, determine the number of rows in the map
		//
		//get info from the image node, the images source path
		//get info from the layer node
		//get info from the data node, store the encoding data in the encodingtype string
		//Trim removes lineseperators or paragraphseperators, so when you Trim each element, it should not contain any whithspace characters at the beginning or end.
		//clear the vector3 list called turretPositions
		//if objectgroupnode and the next descendant element is object. get the x and y values in the object node, divide by poixels per unit 
		//Add these values to the end of the turretpositions array.
		//Advance the XmlReader to the next matching sibling element named object.


        {
			
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;


                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));


                reader.ReadToFollowing("layer");


                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                mapDataString = reader.ReadElementContentAsString().Trim();


                turretPositions.Clear();
				cactusPositions.Clear();
				if (reader.ReadToFollowing("objectgroup")) {
					if (reader.ReadToDescendant("object") ) {
                        do {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

				//NEW STUFF////////////////////////////////////////////////////////////////////////////////////////////

				if (reader.ReadToFollowing("objectgroup")) {
					if (reader.ReadToDescendant("object")) {
						do {
							float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
							float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
							cactusPositions.Add(new Vector3(x, -y, 0));

						} while (reader.ReadToNextSibling("object"));
					}
				}
            


            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {

            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




		// create a new 2d texture with a width and height of 2, load the image from the streaming assets folder and read the data, open the binary file, reads the contents of the file into a byte array, and then closes the file. set the image filtermode to point, clamp the texture to the last pixel at the border.
		//This is useful for preventing wrapping artifacts when mapping an image onto an object and you don't want the texture to tile
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


		// clear the objects in the mapsprites list, loop through the array and cut up spritesheet
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

		// clear the objects in the mapsprites list, loop through the array and cut up spritesheet
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // instantiate turrets at each position in the array/list, set their names to turret,set the turret's parent transform to turretholder
		//Get the DateTime object that is set to the current date and time on this computer, expressed as the local time. print out the current hour, minute and second
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }


		//NEW STUFF////////////////////////////////////////////////////////////////////////////////////////////
		{
			foreach (Vector3 cactusPosition in cactusPositions) {
				GameObject cactus = Instantiate(cactusPrefab, cactusPosition + mapCenterOffset, Quaternion.identity) as GameObject;
				cactus.name = "Cactus";
				cactus.transform.parent = cactiiHolder;
			}
		}


        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}
}


